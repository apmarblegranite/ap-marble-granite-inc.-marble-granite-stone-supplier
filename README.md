AP Marble & Granite is a successful supplier of beautiful marble and other stones from across the world. The idea of establishing a facility to share the beauty of Italy’s natural stone.

Address: 44660 Enterprise Dr, Clinton Township, MI 48038, USA

Phone: 586-783-9434

Website: [https://www.apmarblegranite.com](https://www.apmarblegranite.com)
